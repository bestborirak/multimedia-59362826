import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForestPage } from './forest';

@NgModule({
  declarations: [
    ForestPage,
  ],
  imports: [
    IonicPageModule.forChild(ForestPage),
  ],
})
export class ForestPageModule {}
