import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ForestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forest',
  templateUrl: 'forest.html',
})
export class ForestPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForestPage');
  }
  slider = [
    {
      title: '1. ป่าดิบเมืองร้อน (Tropical evergreen forest)',
      description: 'เป็นป่าที่อยู่ในเขตลมมรสุมพัดผ่านเกือบตลอดปี มีปริมาณน้ำฝนมาก แบ่งออกเป็น ',
      image: "assets/icon/f.svg"
    },
    {
      title: '1.1 ป่าาดงดิบชื้น (Tropical rain forest)',
      description: 'ป่าดงดิบชื้นขึ้นอยู่ในที่ราบรือบนภูเขาที่ระดับความสูงไม่เกิน 600 เมตรจากระดับน้ำทะเล ในภาคใต้พบได้ตั้งแต่ตอนล่างของจังหวัดประจวบคีรีขันธ์ลงไปจนถึงชายเขตแดน ส่วนทางภาคตะวันออกพบในจังหวัดตราด จันทบุรี ระยอง และบางส่วนของจังหวัดชลบุรี ',
      image: "assets/imgs/Tropical.jpg"
    },
    {
      title: '1.2 ป่าดงดิบแล้ง (Dry evergreen forest)',
      description: ' มีอยู่ทั่วไปตามภาคต่าง ๆ ของประเทศ ตามที่ราบเรียบหรือตามหุบเขา มีความสูงจากระดับน้ำทะเลประมาณ 500 เมตร และมีปริมาณน้ำฝนระหว่าง 1,000-1,500 ม.ม. พันธุ์ไม้ที่สำคัญ เช่น ยางแดง มะค่าโมง เป็นต้น พื้นที่ป่าชั้นล่างจะไม่หนาแน่นและค่อนข้างโล่งเตียน',
      image: "assets/imgs/Dry_Tropical.jpg"
    },
    {
      title: '1.3 ป่าดงดิบเขา (Hill evergreen forest)',
      description: 'เป็นป่าที่อยู่สูงจากระดับน้ำทะเล ตั้งแต่ 1,000 เมตรขึ้นไป ส่วนใหญ่อยู่บนเทือกเขาสูงทางภาคเหนือ และบางแห่งในภาคกลางและภาคตะวันออกเฉียงเหนือ พืชที่สำคัญได้แก่ไม้วงศ์ก่อ เช่น ก่อสีเสียด ก่อตาหมูน้อย อบเชย มีป่าเบญจพรรณด้วย เป็นต้น',
      image: "assets/imgs/hill.jpg"
    },
    {
      title: '2. ป่าสน (Coniferous forest)',
      description: 'เป็นสังคมป่าที่อยู่ถัดจากบริเวณสังคมป่าชายเลน โดยอาจจะเป็นพื้นที่ลุ่มที่มีการทับถมของซากพืชและอินทรียวัตถุที่ไม่สลายตัว และมีน้ำท่วมขังหรือชื้นแฉะตลอดปี เช่น จังหวัดเชียงใหม่ ศรีสะเกษ สุรินทร์ ',
      image: "assets/icon/forest.svg"
    },
    {
      title: '3. ป่าพรุหรือป่าบึง (Swamp forest)',
      description: 'พบตามที่ราบลุ่มมีน้ำขังอยู่เสมอ และตามริมฝั่งทะเลที่มีโคลนเลนทั่วๆ ไป แบ่งออกเป็น',
      image: "assets/imgs/swamp.jpg"
    },
    {
      title: '3.1 ป่าพรุ (Peat Swamp)',
      description: 'เป็นสังคมป่าที่อยู่ถัดจากบริเวณสังคมป่าชายเลน โดยอาจจะเป็นพื้นที่ลุ่มที่มีการทับถมของซากพืชและอินทรียวัตถุที่ไม่สลายตัว และมีน้ำท่วมขังหรือชื้นแฉะตลอดปี',
      image: "assets/imgs/peat.jpg"
    },
    {
      title: '3.2 ป่าชายเลน (Mangrove swamp forest)',
      description: 'เป็นสังคมป่าไม้บริเวณชายฝั่งทะเลในจังหวัดทางภาคใต้ กลาง และภาคตะวันออก และมีน้ำขึ้น-น้ำลงอย่างเด่นชัดในรอบวัน',
      image: "assets/imgs/Mangrove.jpg"
    },
    {
      title: '4. ป่าชายหาด (Beach forest)',
      description: 'แพร่กระจายอยู่ตามชายฝั่งทะเลที่เป็นดินกรวด ทราย และโขดหิน ดินมีฤทธิ์เป็นด่าง',
      image: "assets/imgs/beach.jpg"
    }]
}
