import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DeciduousPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deciduous',
  templateUrl: 'deciduous.html',
})
export class DeciduousPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeciduousPage');
  }
  slider = [
    {
      title: '1. ป่าเบญจพรรณ',
      description: 'ลักษณะทั่วไปเป็นป่าโปร่ง พื้นที่ป่าไม้ไม่รกทึบ มีไม้ไผ่ชนิดต่างๆ ขึ้นอยู่มาก มีอยู่ทั่วไปตามภาคต่างๆ ที่เป็นที่ราบ หรือตามเนินเขา พันธุ์ไม้จะผลัดใบในฤดูแล้ง การกระจายของป่าเบญจพรรณในประเทศไทย พบในภาคเหนือ ภาคกลาง และภาคอีสาน',
      image: "assets/imgs/benja.jpg"
    },
    {
      title: '2. ป่าแดง ป่าแพะ หรือป่าเต็งรัง',
      description: 'ลักษณะเป็นป่าโปร่ง มีต้นไม้ขนาดเล็ก และขนาดกลาง ไม้เด่นอันเป็นไม้ดัชนีประกอบด้วยไม้ในวงศ์ยาง ฤดูแล้งจะผลัดใบ และมีไฟป่าเป็นประจำ ป่าชนิดนี้เป็นสังคมพืชเด่นในทางภาคตะวันออกเฉียงเหนือ ส่วนใหญ่ปรากฏสลับกัน ',
      image: "assets/imgs/teng.jpg"
    },
    {
      title: '3. ป่าหญ้า',
      description: 'เกิดจากการทำลายสภาพป่าไม้ที่อุดมสมบูรณ์ ดินมีความเสื่อมโทรม มีฤทธิ์เป็นกรด ต้นไม้ไม่สามารถเจริญเติบโตได้ จึงมีหญ้าต่างๆ เข้าไปแทนที่ แพร่กระจายทั่วประเทศในบริเวณที่ป่าถูกทำลายและเกิดไฟป่าเป็นประจำทุกปี ',
      image: "assets/imgs/paya.jpg"
    },
  ]
}
