import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeciduousPage } from './deciduous';

@NgModule({
  declarations: [
    DeciduousPage,
  ],
  imports: [
    IonicPageModule.forChild(DeciduousPage),
  ],
})
export class DeciduousPageModule {}
