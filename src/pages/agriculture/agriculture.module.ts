import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgriculturePage } from './agriculture';

@NgModule({
  declarations: [
    AgriculturePage,
  ],
  imports: [
    IonicPageModule.forChild(AgriculturePage),
  ],
})
export class AgriculturePageModule {}
