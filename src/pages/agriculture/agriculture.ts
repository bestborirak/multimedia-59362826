import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ForestPage } from '../forest/forest';
import { DeciduousPage } from '../deciduous/deciduous';
import { ProblemPage } from '../problem/problem';
import { ConservePage } from '../conserve/conserve';

/**
 * Generated class for the AgriculturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agriculture',
  templateUrl: 'agriculture.html',
})
export class AgriculturePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgriculturePage');
  }
  GotoForestPage(){
    this.navCtrl.push(ForestPage);
  }
  GotoDeciduousPage(){
    this.navCtrl.push(DeciduousPage);
  }
  GotoProblemPage(){
    this.navCtrl.push(ProblemPage);
  }
  GotoConserve(){
    this.navCtrl.push(ConservePage);
  }
}
