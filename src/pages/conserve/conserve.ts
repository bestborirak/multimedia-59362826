import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConservePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-conserve',
  templateUrl: 'conserve.html',
})
export class ConservePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConservePage');
  }
  slider = [
    {
      title: 'ป้องกันการตัดไม้',
      description: 'ป้องกันและปราบปรามการลักลอบตัดไม้ทำลายป่า',
      image: "assets/icon/axe.svg"
    },
    {
      title: 'หาที่ทำมาหากินที่เหมาะสม',
      description: 'หาแหล่งทำมาหากินให้ชาวเขาอยู่เป็นหลักแหล่ง เพื่อเป็นการป้องกันการทำไร่เลื่อนลอย',
      image: "assets/icon/hand.svg"
    },
    {
      title: 'กำหนดเขตหวงห้าม',
      description: 'ปิดป่าไม่อนุญาตให้มีการทำไม้เพื่อเป็นการอนุรักษ์พื้นป่า ',
      image: "assets/icon/stop.svg"
    },
    {
      title: 'งดการใช้ไม้',
      description: 'ใช้วัตถุอื่นทดแทนผลิตภัณฑ์ที่ทำจากไม้ ',
      image: "assets/icon/furniture-and-household.svg"
    },
    {
      title: 'ตั้งหน่วยป้องกันไฟป่า',
      description: 'มีผู้รับผิดชอบในการป้องกันและปราบปรามการรุกไหม้ของไฟป่า',
      image: "assets/icon/campfire.svg"
    },
    {
      title: 'เสริมด้านการให้ความรู้',
      description: 'ส่งเสริมให้มีการเผยแพร่ความรู้และความเข้าใจแกประชาชน เพื่อให้เห็นความสำคัญของป่าไม้',
      image: "assets/icon/knowledge.svg"
    },
    
  ]
}
