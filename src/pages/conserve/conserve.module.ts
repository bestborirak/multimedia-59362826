import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConservePage } from './conserve';

@NgModule({
  declarations: [
    ConservePage,
  ],
  imports: [
    IonicPageModule.forChild(ConservePage),
  ],
})
export class ConservePageModule {}
